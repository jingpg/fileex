package com.fileex.frontend;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;

public class App {

    public static void main(String[] args) {
        // 当前文件系统类
        FileSystemView fsv = FileSystemView.getFileSystemView();
        // 列出所有windows 磁盘
        File[] fs = File.listRoots();
        // 显示磁盘卷标
        for (int i = 0; i < fs.length; i++) {
//            fsv.get
            System.out.println(fsv.getSystemDisplayName(fs[i]));
        }
    }

}
